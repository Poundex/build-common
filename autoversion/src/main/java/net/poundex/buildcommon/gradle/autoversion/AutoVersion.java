package net.poundex.buildcommon.gradle.autoversion;

import io.vavr.control.Try;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

public class AutoVersion {
	
	public static String release(Path gradleProperties) {
		return new AutoVersion(gradleProperties, new ReleaseVersionCalculator()).getNewVersion();
	}

	public static String snapshot(Path gradleProperties, Path gitRepo) {
		return new AutoVersion(gradleProperties,
				new SnapshotVersionCalculator(gitRepo)).getNewVersion();
	}
	
	private final String newVersion;

	private AutoVersion(Path gradleProperties, VersionCalculator versionCalculator) {
		this.newVersion = writeVersion(gradleProperties, versionCalculator.calculateVersion());
	}

	private String writeVersion(Path gradleProperties, String version) {
		Properties p = new Properties();
		Try.withResources(() -> Files.newBufferedReader(gradleProperties))
				.of(r -> {
					p.load(r);
					return p;
				})
				.get();
		p.setProperty("version", version);
		Try.withResources(() -> Files.newBufferedWriter(gradleProperties))
				.of(w -> {
					p.store(w, null);
					return null;
				})
				.get();
		return version;
	}

	private String getNewVersion() {
		return newVersion;
	}
}
