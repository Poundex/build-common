package net.poundex.buildcommon.gradle.autoversion;

import io.vavr.collection.Stream;
import io.vavr.collection.Traversable;
import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;

import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
class SnapshotVersionCalculator implements VersionCalculator {
	
	private final Path gitRepo;
	
	@Override
	public String calculateVersion() {
		return Try.withResources(() -> Git.open(gitRepo.toFile()))
				.of(this::doCalculateVersion)
				.get();
	}

	@SuppressWarnings("deprecation")
	private String doCalculateVersion(Git git) {
		Option<Map.Entry<String, Ref>> lastTag = Option.ofOptional(git.getRepository().getTags().entrySet().stream()
				.filter(tag -> ! (tag.getKey().contains("-") || tag.getKey().contains("+")))
				.findFirst());

		Option<Integer> commitsSinceLastTag = lastTag.toTry().mapTry(lt ->
						git.log().addRange(
								lt.getValue().getObjectId(),
								git.getRepository().resolve("HEAD")).call())
				.map(Stream::ofAll)
				.map(Traversable::size)
				.toOption();

		String currentBranch = Optional.of(System.getenv("CI_COMMIT_BRANCH"))
				.or(() -> Optional.ofNullable(System.getenv("CI_MERGE_REQUEST_SOURCE_BRANCH_NANE")))
				.map(this::cleanBranchForVersion)
				.map(s -> "branch-" + s)
				.orElse("");

		return lastTag.map(Map.Entry::getKey).getOrElse("0.0.1") +
				"-" + currentBranch +
				commitsSinceLastTag.map(n -> ".c" + n).getOrElse("") +
				".g" + System.getenv("CI_COMMIT_SHORT_SHA");
	}

	private String cleanBranchForVersion(String branch) {
		return branch
				.replaceAll("/", "-")
				.replaceAll("_", "-")
				.replaceAll("[^0-9A-Za-z-]", "");
	}
}
