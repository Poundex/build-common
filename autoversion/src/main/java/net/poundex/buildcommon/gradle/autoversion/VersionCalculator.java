package net.poundex.buildcommon.gradle.autoversion;

interface VersionCalculator {
	String calculateVersion();
}
