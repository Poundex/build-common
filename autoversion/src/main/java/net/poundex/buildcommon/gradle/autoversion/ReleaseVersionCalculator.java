package net.poundex.buildcommon.gradle.autoversion;

class ReleaseVersionCalculator implements VersionCalculator {
	@Override
	public String calculateVersion() {
		return System.getenv("CI_COMMIT_TAG");
	}
}
