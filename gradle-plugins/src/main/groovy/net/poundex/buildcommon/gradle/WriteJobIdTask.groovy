package net.poundex.buildcommon.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

import java.nio.file.Files

class WriteJobIdTask extends DefaultTask {
	
	@Input
	String jobName
	
	@TaskAction
	void run() {
		Files.createDirectories(project.buildDir.toPath())
		project.buildDir.toPath().resolve(String.format("%s.jobid", jobName)).text = 
				System.getenv("CI_JOB_ID")
	}
}
