package net.poundex.buildcommon.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

import java.nio.file.Files
import org.apache.commons.codec.digest.DigestUtils

import java.nio.file.Path

class PreparePackageTask extends DefaultTask {
	
	@Input
	String packageName
	
	@Input
	String artefactName
	
	@Input
	String binaryName
	
	@TaskAction
	void run() {

		Path p = project.buildDir.toPath().resolve("package/${packageName}")
		Files.createDirectories(p)

		String nativex86JobId = project.buildDir.toPath().resolve("${artefactName}.nativex86.jobid").text
		String nativeaarchJobId = project.buildDir.toPath().resolve("${artefactName}.nativeaarch.jobid").text

		String checksumx86
		String checksumaarch
		project.buildDir.toPath().resolve("native/amd64/${binaryName}").withInputStream { is ->
			checksumx86 = DigestUtils.sha256Hex(is)
		}
		project.buildDir.toPath().resolve("native/aarch64/${binaryName}").withInputStream { is ->
			checksumaarch = DigestUtils.sha256Hex(is)
		}

		p.resolve("PKGBUILD").text = project.projectDir.toPath().resolve("package/${packageName}/PKGBUILD").text
				.replaceAll("#VERSION#", project.version.toString().replaceAll('-', ''))
				.replaceAll("#URLx86#", "https://gitlab.com/poundex/${System.getenv("CI_PROJECT_NAME")}/-/jobs/${nativex86JobId}/artifacts/raw/${project.name}/build/native/amd64/${binaryName}")
				.replaceAll("#CHECKSUMx86#", checksumx86)
				.replaceAll("#URLaarch#", "https://gitlab.com/poundex/${System.getenv("CI_PROJECT_NAME")}/-/jobs/${nativeaarchJobId}/artifacts/raw/${project.name}/build/native/aarch64/${binaryName}")
				.replaceAll("#CHECKSUMaarch#", checksumaarch)
	}
}

