package net.poundex.buildcommon.gradle

import groovy.json.JsonOutput
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.TaskAction

import java.nio.file.Path
import java.nio.file.Paths

class WriteArtefactInfoTask extends DefaultTask {
	
	@Input
	String artefactName
	@Input
	String title
	@InputFile
	Path artefactPath
	
	@TaskAction
	void run() {
		Path out = project.buildDir.toPath().resolve("${name}.artefact.json")
		Path projectDirPath = project.rootDir.toPath()
		Path artefact = projectDirPath.relativize(artefactPath)
		
		out.text = JsonOutput.toJson([
				name: title,
				url: "https://gitlab.com/Poundex/${System.getenv("CI_PROJECT_NAME")}/-/jobs/${System.getenv("CI_JOB_ID")}/artifacts/raw/${artefact}"
		])
	}
}
